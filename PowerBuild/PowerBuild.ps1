﻿#
# Script.ps1
#
$configuration = "Release"
$packageLibAssemblyPath = "$PSScriptRoot\..\NugetPackageLib\NugetPackageLib\bin\$configuration\NugetPackageLib.dll"
[System.Reflection.Assembly]::LoadFrom($packageLibAssemblyPath);

cd "$PSScriptRoot\..\OntologyClasses"
Get-Location
& C:\Nuget\nuget.exe pack "OntologyClasses.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\ElasticSearchNestConnector\ElasticSearchNestConnector"
Get-Location
& C:\Nuget\nuget.exe pack "ElasticSearchNestConnector.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\OntologyAppDBConnector"
Get-Location
& C:\Nuget\nuget.exe pack "OntologyAppDBConnector.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

$generatePath = "$PSScriptRoot\..\GenerateOntoCode\GenerateOntoCode\bin\Release"
cd $generatePath
& .\GenerateOntoCode.exe

$nugetId = "OntologyModulesItems"
$specPath = "$PSScriptRoot\$nugetId.nuspec"
$description = "Autogenerated Classes for Ontology-Modules"

$version = [System.Diagnostics.FileVersionInfo]::GetVersionInfo("$generatePath\OModules.Channels.dll").FileVersion

$package = New-Object NugetPackageLib.PackageCreator($version, $nugetId, "Tassilo Koller", $description)
$package.AddFile("$generatePath\OModules.Channels.dll", "lib\net461\OModules.Channels.dll")
$package.AddFile("$generatePath\OModules.Commands.dll", "lib\net461\OModules.Commands.dll")
$package.SaveSpec($specPath)
$package.PackPackage("c:\nuget\nuget.exe", $specPath)

cd "$PSScriptRoot\..\ImportExport-Module\ImportExport-Module"
Get-Location
& C:\Nuget\nuget.exe pack "ImportExport-Module.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\OntoMsg-Module\OntoMsg-Module"
Get-Location
& C:\Nuget\nuget.exe pack "OntoMsg-Module.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\Structure-Module\Structure-Module"
Get-Location
& C:\Nuget\nuget.exe pack "Structure-Module.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\OntologyViewModels\OntologyViewModels"
Get-Location
& C:\Nuget\nuget.exe pack "OntologyViewModels.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\OntoWebCore"
Get-Location
& C:\Nuget\nuget.exe pack "OntoWebCore.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot

cd "$PSScriptRoot\..\OntologyClassEdit\OntologyClassEdit"
Get-Location
& C:\Nuget\nuget.exe pack "OntologyClassEdit.csproj" -Prop Configuration=Release -OutputDirectory $PSScriptRoot


