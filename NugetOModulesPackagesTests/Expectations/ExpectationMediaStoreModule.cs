﻿using MediaStore_Module;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetOModulesPackagesTests.Expectations
{
    public static class ExpectationMediaStoreModule
    {
        private static Globals globals;
        public static Globals Globals
        {
            get
            {
                if (globals == null)
                {
                    globals = new Globals();
                }

                return globals;
            }
        }

        public static string Path1
        {
            get
            {
                return @"\\wdmycloudmirror\Drachenheim\Videos\14_Tagebuecher_des_Ersten_Weltkriegs_14.04.29_21-10_arte_60_TVOON_DE.mpg.HD.cut.mp4";
            }
        }

        public static bool DoCreate
        {
            get
            {
                return true;
            }
        }
        public static FileWorkManager GetFileWorkManager()
        {
            return new FileWorkManager();
        }
    }
}
