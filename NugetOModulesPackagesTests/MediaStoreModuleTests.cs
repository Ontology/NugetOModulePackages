﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NugetOModulesPackagesTests.Expectations;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetOModulesPackagesTests
{
    [TestClass]
    public class MediaStoreModuleTests
    {
        [TestMethod]
        public void TestGetFileSystemObjectByPath()
        {
            var fileWorkManager = ExpectationMediaStoreModule.GetFileWorkManager();
            var result = fileWorkManager.GetFileSystemObjectByPath(ExpectationMediaStoreModule.Path1, ExpectationMediaStoreModule.DoCreate);
            if (result.GUID_Related == ExpectationMediaStoreModule.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while getting the FileSystemObject");
            }
        }
    }
}
